import 'package:flutter/material.dart';
import 'package:web_to_mobile_app/web_to_app.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       home: WebToApp(),
    );
  }
}

