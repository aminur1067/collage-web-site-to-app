import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class WebToApp extends StatefulWidget {
  const WebToApp({Key? key}) : super(key: key);

  @override
  State<WebToApp> createState() => _WebToAppState();
}

class _WebToAppState extends State<WebToApp> {

  late InAppWebViewController webViwe;
  double _progress = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse("https://www.riitpolybd.com/")),
          onWebViewCreated: (InAppWebViewController controller){
            webViwe=controller;
          },
    onProgressChanged: (InAppWebViewController controller,int progress){
            setState(() {
              _progress=progress / 100;
            });
    },
          onLoadStart: ((controller, url){}


        ),

        ),
        _progress < 1 ? SizedBox(height: 3,child: LinearProgressIndicator(
          value: _progress,

        backgroundColor: Colors.orange.withOpacity(0.2),
        ),) : const SizedBox()
    ]
    );
  }
}
